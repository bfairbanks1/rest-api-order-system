Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
POST | /shutdown | | | Shuts down the server

**Base Assignment:**

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
GET | /customers | | ex:[ { "number": 1, "firstName": "TestName", "lastName": "Number 1" }, { "number": 2, "firstName": "TestName" "lastName": "Number 2" }, { "number": 3, "firstName": "TestName", "lastName": "Number 3" } ] | Returns an array of the Customer Objects that have been added
PUT | /customers/*number*/change-name | {"firstName" : *newFirstName*, "lastName" : *newLastName*} | ex: "Customer *customerNumber* name has been changed" | Returns confirmation that name has been changed

**Intermediate Add-On:**

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
GET | /customers/get-number | {"firstName" : *firstName*, "lastName" : *lastName*} | *customerNumber* | Takes a Customer name and returns their number if found
GET | /orders/numbers-by-customer/{customerNum} | | {*orderNum1, orderNum2, etc*} | Returns all the order numbers associated with a particuar customer number


**Advanced Add-On:**<br/>Describe what it does: Prints a catalog of all products available<br/>What "tables" does it use: It uses just the productDb "table"

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
GET | /products | | ex: Product catalog<br/> SKU	Description  Unit Price <br/>1		Product1		4.99<br/>2		Product2		4.99<br/>3		Product3		4.99<br/>4		Product4		4.99 | Returns formatted catalog

package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import jdk.nashorn.internal.objects.annotations.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    }

    /**
     * Intermediate Add-on - Get Customer's number from their first and last name
     * @param customer - Placeholder Customer with first and last names to be checked against
     * @return Customer's number if found in DB, otherwise NOT_FOUND
     */
    @GetMapping("customers/get-number")
    public ResponseEntity<Object> getCustomerNumber(@RequestBody Customer customer){
        List<Customer> customerList = new ArrayList<>(customerDb.values());
        for(Customer currCustomer : customerList)
            if(currCustomer.getFirstName().equals(customer.getFirstName()) && currCustomer.getLastName().equals(customer.getLastName()))
                return new ResponseEntity<>(currCustomer.getNumber(), HttpStatus.OK);
        return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    }

    /**
     * Base Assignment - Change name of existing Customer using Customer number as reference
     * @param number - Customer number
     * @param customer - Customer with new first and last name to replace existing ones
     * @return - Confirmation message of name change, other wise NOT_FOUND
     */
    @CrossOrigin()
    @PostMapping("/customers/{number}/change-name")
    public ResponseEntity<Object> setCustomerName(@PathVariable long number, @RequestBody Customer customer){
        if(customerDb.containsKey(number)){
            customerDb.replace(number, customerDb.get(number), customer);
            return new ResponseEntity<>(("Customer " + number + " name has been changed"), HttpStatus.ACCEPTED);
        }
        else
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    }

    /**
     * Base Assignment - Returns an array of all Customer objects
     * @return - array of all Customer's in customerDb
     */
    @GetMapping("/customers")
    public ResponseEntity<Object> listCustomers(){
        if(!customerDb.isEmpty())
            return new ResponseEntity<>(customerDb.values(), HttpStatus.OK);
        else
            return new ResponseEntity<>("No Customers in Database", HttpStatus.NOT_FOUND);
    }
       
}